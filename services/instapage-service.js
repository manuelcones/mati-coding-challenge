
var request=require('request');

var APP_URL='http://app.instapage.com/api/plugin/page/';


exports.login = function getAllUsers(email,password,callback) {

  var headers={
      integration:'wordpress'
  };
  var data={
        headers:headers,
        form:{
            email:email,
            password:password
        }
    };

request.post(APP_URL,data,function(err,httpResponse,body){

    body=JSON.parse(body);

    if(body.error){ return callback(body,null); }
    return callback(null,body);

})

};

exports.getAccountKeys=function(userToken,callback){

    var headers={
        integration:'wordpress',
        usertoken:userToken,
    };
    var data={
        headers:headers,
    };

    request.post(APP_URL+'get-account-keys',data,function(err,httpResponse,body){


        body=JSON.parse(body);
        if(body.error){ return callback(body,null); }
        return callback(null,body);

    })

};



exports.getSubAccounts=function(accountKeys,callback){

    var headers={
        integration:'wordpress',
        accountkeys:accountKeys,
    };
    var data={
        headers:headers,
    };

    request.post(APP_URL+'get-sub-accounts-list',data,function(err,httpResponse,body){

        body=JSON.parse(body);

        if(body.error){ return callback(body,null); }
        return callback(null,body.data);

    })

};

exports.connectSubAccount=function(accountKey,host,callback){

    var b64Token=JSON.stringify([accountKey]);
    var buffer = new Buffer(b64Token);
    b64Token=buffer.toString('base64');

    var headers={
        integration:'wordpress',
        accountkeys:b64Token,
    };

    var data={
        headers:headers,
        form:{
            status:'connect',
            accountkeys:b64Token,
            domain:host

        }
    };

    request.post(APP_URL+'connection-status',data,function(err,httpResponse,body){

        console.log(err);
        body=JSON.parse(body);
        console.log('connect subaccount',body);

        if(body.error){ return callback(body,null); }
        return callback(null,body.data);

    })

};

exports.getLandingPages=function(accountKeys,subAccountKey,callback){

    console.log('get landing subaccountKey',subAccountKey);

    var b64Token=JSON.stringify([subAccountKey]);
    var buffer = new Buffer(b64Token);
    b64Token=buffer.toString('base64');

    console.log('base 64 token',b64Token);

    var headers={
        integration:'wordpress',
        accountkeys:b64Token,
    };
    var data={
        headers:headers,
    };

    console.log(APP_URL+'list')
    request.get(APP_URL+'list',data,function(err,httpResponse,body){

        body=JSON.parse(body);
        console.log('list response',body);

        if(body.error){ return callback(body,null); }
        return callback(null,body.data);

    })

};