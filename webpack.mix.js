let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix.copyDirectory('frontend/img/', 'public/img');

mix.js('frontend/js/app.js', 'public/js')
    .sass('frontend/sass/app.scss', 'public/css');

mix.sass('frontend/sass/login.scss','public/css');

