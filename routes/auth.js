var express = require('express');
var router = express.Router();

var authController=require('../controllers/auth-controller');

router.get('/login',authController.index);
router.post('/login',authController.doLogin);
router.get('/logout',authController.logout);


module.exports = router;
