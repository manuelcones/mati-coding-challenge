var adminRouter = require('./admin');
var apiRouter = require('./api');
var authRouter=require('./auth');
var publicRouter = require('./public');

var express = require('express');
var router = express.Router();
var instaPageLogin=require('../middlewares/instapage-auth');
var apiAuthentication=require('../middlewares/instapage-auth');

router.use('/admin',instaPageLogin, adminRouter);
router.use('/api/v1/',apiAuthentication, apiRouter);
router.use('/auth', authRouter);
router.use('/', publicRouter);

module.exports = router;
