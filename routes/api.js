var express = require('express');
var router = express.Router();

var apiController=require('../controllers/api-controller');

router.get('/users/me/subaccount',apiController.getSubAccount);
router.get('/landing-pages',apiController.getLandingPages);

module.exports = router;
