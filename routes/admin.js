var express = require('express');
var router = express.Router();

var adminController=require('../controllers/admin-controller');

router.get('/',adminController.index);
router.post('/subaccount-selector',adminController.saveSubAccount);
router.get('/subaccount-selector',adminController.subaccount);


module.exports = router;
