
var instapageService=require('../services/instapage-service');


exports.index = function(req, res) {

    res.render('admin',{user: req.session.user,currentSubAccount:req.session.currentSubAccount,authHeader:req.session.masterToken });

};

exports.subaccount = function(req, res) {

    instapageService.getSubAccounts(req.session.masterToken,function(err,subAccounts){
        res.render('subaccount-selector',{ user: req.session.user,subAccounts:subAccounts });
    });
};

exports.saveSubAccount = function(req, res) {

    req.session.currentSubAccount=JSON.parse(req.body.subaccount);

    instapageService.connectSubAccount(req.session.currentSubAccount.accountkey,'http://mydemo.pagedemo.co',function(err,response){
        res.redirect('/admin');
    });

};
