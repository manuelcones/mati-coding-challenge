
var instapageService=require('../services/instapage-service');


exports.getSubAccount = function(req, res) {

    res.json(req.session.currentSubAccount);
};

exports.getLandingPages = function(req, res) {

    instapageService.getLandingPages(req.session.accountKeys,req.headers.accountkey,function(err,pages){
        res.json(pages);
    });

};