
var instapageService=require('../services/instapage-service');

exports.index = function(req, res) {
    res.render('login');
};

exports.doLogin = function(req, res) {

    // console.log(req.body);

    if(req.body.email && req.body.password){

        instapageService.login(req.body.email,req.body.password,function(err,response){

            if(err){
                return res.render('login',{error:err});
            }else{
                req.session.user=response.data;

                instapageService.getAccountKeys(req.session.user.usertoken,function(err,response){

                    req.session.accountKeys=response.data.accountkeys;
                    var accountKeys=JSON.stringify(response.data.accountkeys);
                    var buffer = new Buffer(accountKeys);
                    req.session.masterToken=buffer.toString('base64');

                    return res.redirect('/admin/subaccount-selector');
                });

            }

        })
    }
};

exports.logout=function(req,res){

    req.session.destroy(function(err) {
        if(err) {
            return next(err);
        } else {
            res.redirect('/auth/login');
        }
    });


};
