# MATI CODING CHALLENGE

mati coding challenge

## Getting Started

 CLONE THIS REPO

### Prerequisites

* HAVE GIT INSTALLED
* HAVE DOCKER AND DOCKER COMPOSE INSTALLED

### Installing

Launch this commands

```
 cp .env.example .env
 docker-compose up --build
```

Project should be running on http://localhost:3000

