
require('./bootstrap');

import BootstrapVue from 'bootstrap-vue'

window.Vue = require('vue');
Vue.use(BootstrapVue);

Vue.component('instapage-manager', require('./components/InstapageManager.vue'));


const app = new Vue({
    el: '#app'
});
